package com.web.module.service;

import com.web.module.domain.TeacherEntity;
import com.web.module.dto.Teacher;
import com.web.module.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImp implements TeacherService{

    @Autowired
    private TeacherRepository teacherRepository;

    @Transactional
    @Override
    public List<Teacher> getAllTeachers() {
        return teacherRepository
                .findAll()
                .stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Teacher getTeacherById(Long id) {
        return entityToDto(teacherRepository.getOne(id));
    }

    private Teacher entityToDto(TeacherEntity teacherEntity) {
        return Teacher.builder()
                .teacherId(teacherEntity.getTeacherId())
                .surName(teacherEntity.getSurName())
                .name(teacherEntity.getName())
                .middleName(teacherEntity.getMiddleName())
                .build();
    }
}
