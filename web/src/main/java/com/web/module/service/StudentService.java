package com.web.module.service;

import com.web.module.dto.Student;

import java.util.List;

public interface StudentService {

    List<Student> getAll();

    Student getById(Long id);

    Student getByName(String name);

    Student create(Student student);

//    Student update(Long id, Student student);

    void delete(Long id);
}
