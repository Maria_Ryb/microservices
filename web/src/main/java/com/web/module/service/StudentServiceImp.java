package com.web.module.service;

import com.web.module.domain.StudentEntity;
import com.web.module.dto.Student;
import com.web.module.kafka.KafkaService;
import com.web.module.repository.StudentRepository;
import com.web.module.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImp implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private KafkaService kafkaService;

    @Transactional
    @Override
    public List<Student> getAll() {
        return studentRepository
                .findAll()
                .stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Student getById(Long id) {
        return entityToDto(studentRepository.getOne(id));
    }

    @Override
    public Student getByName(String name) {
        StudentEntity studentEntity = studentRepository.findByName(name);
        if (studentEntity == null) {
            throw new EntityNotFoundException("Failed to find student with name " + name);
        }
        return entityToDto(studentEntity);
    }

    @Override
    public Student create(Student student) {
        StudentEntity result = studentRepository.save(dtoToEntity(student));
        Student studentDto = entityToDto(result);
        kafkaService.send(studentDto.getId(), studentDto);
        return studentDto;
    }

    @Override
    public void delete(Long id) {
        kafkaService.send(id, null);
        studentRepository.deleteById(id);
    }

    private Student entityToDto(StudentEntity studentEntity) {
        return Student.builder()
                .id(studentEntity.getId())
                .name(studentEntity.getName())
                .surName(studentEntity.getSurName())
                .groupStudent(studentEntity.getGroupStudent())
                .teacherId(studentEntity.getTeacher().getTeacherId())
                .build();
    }

    private StudentEntity dtoToEntity(Student student) {
        return StudentEntity.builder()
                .name(student.getName())
                .surName(student.getSurName())
                .groupStudent(student.getGroupStudent())
                .teacher(teacherRepository.getOne(student.getTeacherId()))
                .build();

    }

}
