package com.web.module.controller;

import com.web.module.dto.Student;
import com.web.module.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping
    public List<Student> getAllStudent() {
        return studentService.getAll();
    }

    @GetMapping("/{id}")
    public Student getStudentByID(@PathVariable(name = "id") Long id) {
        return studentService.getById(id);
    }


    @GetMapping("/{name}")
    public Student getStudentByID(@PathVariable(name = "name") String name) {
        return studentService.getByName(name);
    }

    @PostMapping
    public Student createStudent(@RequestBody Student student) {
        return studentService.create(student);
    }

    @DeleteMapping
    public void deleteStudent(@RequestParam(value = "id", required = true) Long id) {
        studentService.delete(id);
    }

}
