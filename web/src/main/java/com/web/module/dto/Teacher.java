package com.web.module.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Teacher {

    private Long teacherId;

    private String name;

    private String surName;

    private String middleName;

}
