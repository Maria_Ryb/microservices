package com.web.module.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Student {

    private Long id;

    @NotNull(message = "Please enter name")
    private String name;

    @NotNull(message = "Please enter surname")
    private String surName;

    @NotNull(message = "Please enter group")
    private Long groupStudent;

    @NotNull(message = "Please enter teacherId")
    private Long teacherId;

}
