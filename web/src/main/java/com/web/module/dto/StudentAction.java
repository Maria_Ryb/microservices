package com.web.module.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentAction {

    private Long id;

    private Student student;
}
