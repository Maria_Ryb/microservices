package com.web.module.kafka;

import com.web.module.dto.Student;
import com.web.module.dto.StudentAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaServiceImp implements KafkaService {

    @Autowired
    private KafkaTemplate<Long, StudentAction> kafkaTemplate;

    @Override
    public void send(Long id, Student student) {
        kafkaTemplate.send("students", new StudentAction(id, student));
    }

}
