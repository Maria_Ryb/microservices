package com.web.module.kafka;

import com.web.module.dto.Student;

public interface KafkaService {

    void send(Long id, Student student);
}
