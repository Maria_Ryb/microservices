package com.web.module.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TEACHERS")
public class TeacherEntity {

    @Id
    @Column(name = "TEACHER_ID")
    private Long teacherId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SUR_NAME")
    private String surName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;
}
