package com.web.module.domain;



import lombok.*;


import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "STUDENTS")
public class StudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SUR_NAME")
    private String surName;

    @Column(name = "GROUP_STUDENT")
    private Long groupStudent;

    @ManyToOne
    @JoinColumn(name = "TEACHER_ID", nullable = false)
    private TeacherEntity teacher;



}
