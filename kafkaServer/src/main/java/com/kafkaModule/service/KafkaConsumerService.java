package com.kafkaModule.service;

import com.web.module.dto.StudentAction;

public interface KafkaConsumerService {

    void consume(StudentAction studentAction);
}
