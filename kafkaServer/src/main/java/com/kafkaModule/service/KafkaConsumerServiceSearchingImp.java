package com.kafkaModule.service;

import com.kafkaModule.domain.StudentKafkaEntity;
import com.kafkaModule.repository.KafkaRepository;
import com.web.module.dto.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class KafkaConsumerServiceSearchingImp implements KafkaConsumerServiceSearching {

    @Autowired
    private KafkaRepository kafkaRepository;

    @Autowired
    private KafkaTemplate<Long, StudentKafkaEntity> kafkaTemplate;

    @Transactional
    @Override
    public Student getByName(String name) {
        StudentKafkaEntity studentEntity = kafkaRepository.getByName(name);
        return entityToDTo(studentEntity);
    }

    @Transactional
    @Override
    public Student getById(Long id) {
        StudentKafkaEntity studentEntity = kafkaRepository.getOne(id);
        return entityToDTo(studentEntity);
    }

    @Transactional
    @Override
    public Student getBySurname(String surName) {
        StudentKafkaEntity studentEntity = kafkaRepository.getBySurName(surName);
        return entityToDTo(studentEntity);
    }

    private Student entityToDTo(StudentKafkaEntity studentKafkaEntity) {
        return Student.builder()
                .name(studentKafkaEntity.getName())
                .surName(studentKafkaEntity.getSurName())
                .id(studentKafkaEntity.getId())
                .build();
    }
}
