package com.kafkaModule.service;

import com.kafkaModule.domain.StudentKafkaEntity;
import com.kafkaModule.repository.KafkaRepository;
import com.web.module.dto.Student;
import com.web.module.dto.StudentAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class KafkaConsumerServiceImp implements KafkaConsumerService {

    @Autowired
    private KafkaRepository kafkaRepository;

    @Autowired
    private KafkaTemplate<Long, StudentAction> kafkaTemplate;

    @KafkaListener(topics = {"students"}, containerFactory = "singleFactory")
    public void consume(StudentAction studentAction) {
        if (studentAction.getStudent() == null) {
            kafkaRepository.deleteById(studentAction.getId());
        } else {
            kafkaRepository.save(dtoToEntity(studentAction.getStudent()));
        }
    }

    private StudentKafkaEntity dtoToEntity(Student student) {
        return StudentKafkaEntity.builder()
                .name(student.getName())
                .surName(student.getSurName())
                .id(student.getId())
                .build();
    }
}
