package com.kafkaModule.service;

import com.web.module.dto.Student;

public interface KafkaConsumerServiceSearching {

    Student getByName(String name);

    Student getById(Long id);

    Student getBySurname(String surname);
}
