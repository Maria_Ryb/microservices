package com.kafkaModule.controller;

import com.kafkaModule.domain.StudentKafkaEntity;
import com.kafkaModule.repository.KafkaRepository;
import com.kafkaModule.service.KafkaConsumerServiceSearching;
import com.web.module.dto.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private KafkaRepository kafkaRepository;

    @Autowired
    private KafkaConsumerServiceSearching kafkaConsumerServiceSearching;

    @GetMapping
    public List<Student> getAllStudents() {
        return kafkaRepository.findAll()
                .stream()
                .map(this::entityToDTo)
                .collect(Collectors.toList());
    }

    @GetMapping("/name/{name}")
    public Student getStudentKafkaByName(@PathVariable(value = "name") String name) {
        return kafkaConsumerServiceSearching.getByName(name);
    }

    @GetMapping("/id/{id}")
    public Student getStudentKafkaById(@PathVariable(value = "id") Long id) {
        return kafkaConsumerServiceSearching.getById(id);
    }

    @GetMapping("/surName/{surName}")
    public Student getStudentKafkaBySurName(@PathVariable(value = "surName") String surName) {
        return kafkaConsumerServiceSearching.getBySurname(surName);
    }

    private Student entityToDTo(StudentKafkaEntity studentKafkaEntity) {
        return Student.builder()
                .name(studentKafkaEntity.getName())
                .surName(studentKafkaEntity.getSurName())
                .id(studentKafkaEntity.getId())
                .build();
    }
}
