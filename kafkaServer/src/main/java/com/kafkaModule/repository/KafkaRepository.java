package com.kafkaModule.repository;

import com.kafkaModule.domain.StudentKafkaEntity;
import com.web.module.domain.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KafkaRepository extends JpaRepository<StudentKafkaEntity, Long> {

    StudentKafkaEntity getByName(String name);

    StudentKafkaEntity getBySurName(String surName);
}
